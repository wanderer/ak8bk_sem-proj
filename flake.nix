{
  description = "AK8BK sem-proj report";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
  };

  outputs =
    { self
    , nixpkgs
    , ...
    }:
    let
      supportedSystems = [ "x86_64-linux" "x86_64-darwin" ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;

      courseCode = "ak8bk";
      documentName = "sem-proj";
      pname = courseCode + "_" + documentName;
      version = "0.0.1";

      nixpkgsFor = forAllSystems (
        system:
        import nixpkgs {
          inherit system;
          overlays = [ self.overlay ];
        }
      );
    in
    rec {
      overlay = final: prev: {
        watcher = with final;
          pkgs.writeScriptBin "watch" ''
            out=".latexmkout"
            mkdir "$out"
            latexmk \
              -pvc \
              -outdir="$out" \
              -pdf \
              -pdflatex="pdflatex -synctex=1 -file-line-error -interaction=nonstopmode" \
              -use-make ${documentName}.tex
            rm -r "$out"
          '';

        buildLatex = with final;
          pkgs.stdenv.mkDerivation {
            inherit pname version;

            src = lib.cleanSource ./.;

            nativeBuildInputs = with pkgs; [
              (texlive.combine {
                inherit
                  (texlive)
                  scheme-full# needed for authblk
                  # scheme-medium
                  multirow
                  hyperref
                  blindtext
                  fancyhdr
                  etoolbox
                  topiclongtable
                  ;
              })
              gnumake
            ];

            buildPhase = ''
              latexmk \
              -pdf \
              -pdflatex="pdflatex -file-line-error -interaction=nonstopmode" \
              -use-make ${documentName}.tex
            '';
            installPhase = ''
              install -Dm444 -t $out ${documentName}.pdf
            '';
          };
      };

      packages = forAllSystems (system: {
        inherit (nixpkgsFor.${system}) watcher buildLatex;
      });

      defaultPackage =
        forAllSystems (system: self.packages."${system}".buildLatex);

      apps = forAllSystems (system: rec {
        watch = {
          type = "app";
          program = "${self.packages."${system}".watcher}/bin/watch";
        };
        buildLatex = {
          type = "app";
          program = "${self.packages."${system}".buildLatex}";
        };
        # default = buildLatex;
        default = watch;
      });
    };
}
